package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;

/**
 * 生产计划
 * @author wxd56
 *
 */
@Entity
@Table(name="scgl_plant_plan")
public class PlantPlan extends BaseModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1446278890509827564L;
	
	/**
	 * 计划名称
	 */
	@Column(name="name",length = 100)
	@JsonView(JsonViews.SummaryView.class)
	private String name;
	
	/**
	 * 作物
	 */
	@ManyToOne
	@JoinColumn(name="plant_id")
	@JsonView(JsonViews.PlanList.class)
	private PlantInfo plant;
	
	/**
	 * 种植时间
	 */
	@JsonView(JsonViews.PlanList.class)
	@Column(name="plant_date")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date plantDate;
	
	
	/**
	 * 采摘时间
	 */
	@JsonView(JsonViews.PlanList.class)
	@Column(name="pick_date")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date pickDate;
	
	/**
	 * 负责人
	 */
	@ManyToOne
	@JoinColumn(name="owner_user_id")
	@JsonView(JsonViews.PlanList.class)
	private SysUser user;
	
	@Column(name="pre_product")
	@JsonView(JsonViews.PlanList.class)
	private BigDecimal preProduct;
	
	/**
	 * 优先级
	 */
	@JsonView(JsonViews.PlanList.class)
	@Column(name="priority")
	private Integer priority;
	
	/**
	 * 备注
	 */
	@Column(name="remark",length = 100)
	@JsonView(JsonViews.PlanList.class)
	private String remark;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PlantInfo getPlant() {
		return plant;
	}

	public void setPlant(PlantInfo plant) {
		this.plant = plant;
	}

	public Date getPickDate() {
		return pickDate;
	}

	public void setPickDate(Date pickDate) {
		this.pickDate = pickDate;
	}

	public SysUser getUser() {
		return user;
	}

	public void setUser(SysUser user) {
		this.user = user;
	}

	public BigDecimal getPreProduct() {
		return preProduct;
	}

	public void setPreProduct(BigDecimal preProduct) {
		this.preProduct = preProduct;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getPlantDate() {
		return plantDate;
	}

	public void setPlantDate(Date plantDate) {
		this.plantDate = plantDate;
	}
	

}
