package com.mengtian_tech.smart_farm.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.model.SysUser;
import com.mengtian_tech.smart_farm.service.SystemService;
import com.mengtian_tech.smart_farm.vo.AjaxJson;
import com.mengtian_tech.smart_farm.vo.JsonViews;
import com.mengtian_tech.smart_farm.vo.PageResult;
import com.mengtian_tech.smart_farm.vo.Query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * 地块信息控制器
 * @author wxd56
 *
 */
@Controller
@RequestMapping("/xtgl/user/")
public class SysUserController {

	@Autowired
	SystemService systemService;
	
	@RequestMapping("list")
	@ResponseBody
	public AjaxJson list(@RequestBody(required = false) SysUser obj) {
		if(obj == null) obj = new SysUser();
		Query<SysUser> q = Query.create(obj);
		PageResult<SysUser> result =  this.systemService.findPageList(q);		
		AjaxJson json = AjaxJson.getOk(result);
		return json;
	}
	
	
	/**
	 * 下拉框
	 * @param obj
	 * @return
	 */
	@RequestMapping("selectList")
	@ResponseBody
	@JsonView(JsonViews.SummaryView.class) //json视图，只是获取简单的name属性
	public AjaxJson selectList(@RequestBody(required = false) SysUser obj) {
		if(obj == null) obj = new SysUser();
		Query<SysUser> q = Query.create(obj);
		List<SysUser> result =  this.systemService.findList(q);		
		AjaxJson json = AjaxJson.getOk(result);
		return json;
	}
	
	
	
	/**
	 * 保存
	 */
	@RequestMapping("save")
	@ResponseBody
	public AjaxJson save(@RequestBody SysUser user) {
		
		//检查是否登录名已经存在
		Query<SysUser> q = Query.create(SysUser.class);
		q.notEq("id",user.getId());
		q.eq("loginName", user.getLoginName());
		boolean exist = this.systemService.exist(q);
		if(exist) {
			return AjaxJson.getError("用户名称已经存在！");
		}
		
		this.systemService.save(user);
		AjaxJson json = AjaxJson.getOk("保存成功");
		return json;
	}
	
	
	/**
	 * 删除
	 */
	@RequestMapping("delete")
	@ResponseBody
	public AjaxJson delete(@RequestBody SysUser user) {
		this.systemService.delete(user);
		AjaxJson json = AjaxJson.getOk("删除成功");
		return json;
	}
	
	
}
