package com.mengtian_tech.smart_farm.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;


/**
 * 基地信息
 * @author wxd56
 *
 */
@Entity
@Table(name="zygl_base_info")
public class BaseInfo extends BaseModel{
	/**
	 * 基地名称
	 */
	@JsonView(JsonViews.SummaryView.class)
	@Column(name="name",length = 150)
	private String name;
	
	/**
	 * 中心点坐标
	 */
	@Column(name="latitude",length = 20,precision =20,scale = 16 )
	private BigDecimal latitude;
	
	/**
	 * 中心点坐标
	 */
	@Column(name="longitude",length = 20,precision =20,scale = 16 )
	private BigDecimal longitude;
	
	/**
	 * 联系人
	 */
	@Column(name="contact_user",length = 80)
	private String contactUser;
	
	/**
	 * 联系电话
	 */
	@Column(name="contact_phone",length = 30)
	private String contactPhone;
	
	/**
	 * 做标集合
	 */
	@Lob
	@Column(name="coordinate_group")
	private String coordinateGroup;
	
	/**
	 * 区域颜色
	 */
	@Column(name="view_color",length = 50)
	private String viewColor;
	
	/**
	 * 面积（亩）
	 */
	@Column(name="mu_number")
	private BigDecimal muNumber;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public String getContactUser() {
		return contactUser;
	}

	public void setContactUser(String contactUser) {
		this.contactUser = contactUser;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getCoordinateGroup() {
		return coordinateGroup;
	}

	public void setCoordinateGroup(String coordinateGroup) {
		this.coordinateGroup = coordinateGroup;
	}

	public String getViewColor() {
		return viewColor;
	}

	public void setViewColor(String viewColor) {
		this.viewColor = viewColor;
	}

	public BigDecimal getMuNumber() {
		return muNumber;
	}

	public void setMuNumber(BigDecimal muNumber) {
		this.muNumber = muNumber;
	}
	
	
	
	
}
