package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;
/**
 * 分区子类型
 * @author wxd56
 *
 */
@Entity
@Table(name="dict_partition_sub_type")
public class DictPartitionSubType extends BaseModel implements Serializable{

	private static final long serialVersionUID = 465908131127791453L;
	
	/**
	 * 类型名称
	 */
	@Column(name="name",length = 200)
	@JsonView(JsonViews.SummaryView.class)
	private String name;
	
	/**
	 * 排列序号
	 */
	@Column(name="sort_order")
	private Integer sortOrder;
	
	@ManyToOne
	@JoinColumn(name="parent_id")
	private DictPartitionType parent; 

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public DictPartitionType getParent() {
		return parent;
	}

	public void setParent(DictPartitionType parent) {
		this.parent = parent;
	}	

}
