package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;

/**
 * 日常巡检
 */
@Entity
@Table(name="scgl_daily_check")
public class DailyCheck extends BaseModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4204748611374462L;

	 
	/**
	 * 地块
	 */
	@ManyToOne
	@JoinColumn(name="field_id")
	@JsonView(JsonViews.SummaryView.class)
	private FieldInfo field;
	
	/**
	 * 巡检时间
	 */
	@Column(name="check_date")
	@JsonFormat(pattern = "yyyy-MM-dd")
	@JsonView(JsonViews.SummaryView.class)
	private Date checkDate;
	
	
	/**
	 * 负责人
	 */
	@ManyToOne
	@JoinColumn(name="user_id")
	@JsonView(JsonViews.SummaryView.class)
	private SysUser user;
	
	/**
	 * 巡检状态
	 */
	@Column(name="check_state")
	@JsonView(JsonViews.SummaryView.class)
	private Integer state;
	
	 
	
	/**
	 * 空气温度
	 */
	@Column(name="air_temp")
	@JsonView(JsonViews.SummaryView.class)
	private Float airTemp;
	

	/**
	 * 土壤温度
	 */
	@Column(name="solid_temp")
	@JsonView(JsonViews.SummaryView.class)
	private Float solidTemp;
	

 
	    
	

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	public FieldInfo getField() {
		return field;
	}

	public void setField(FieldInfo field) {
		this.field = field;
	}

	public SysUser getUser() {
		return user;
	}

	public void setUser(SysUser user) {
		this.user = user;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Float getAirTemp() {
		return airTemp;
	}

	public void setAirTemp(Float airTemp) {
		this.airTemp = airTemp;
	}

	public Float getSolidTemp() {
		return solidTemp;
	}

	public void setSolidTemp(Float solidTemp) {
		this.solidTemp = solidTemp;
	}
	
	
	
	
}
