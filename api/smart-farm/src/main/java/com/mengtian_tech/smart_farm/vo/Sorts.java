package com.mengtian_tech.smart_farm.vo;

public class Sorts {
	/**
	 * 升序排列
	 */
	public static final String ASC = "asc";
	
	/**
	 * 降序排列
	 */
	public static final String DESC = "desc";
}
