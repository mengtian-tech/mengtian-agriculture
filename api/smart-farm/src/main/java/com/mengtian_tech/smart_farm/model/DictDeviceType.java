package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;

/**
 * 设备类型字典表
 */
@Entity
@Table(name="dict_device_type")
public class DictDeviceType extends BaseModel implements Serializable{

	private static final long serialVersionUID = 37791453L;
	
	/**
	 * 类型名称
	 */
	@Column(name="name",length = 200)
	@JsonView(JsonViews.SummaryView.class)
	private String name;
	
	/**
	 * 排列序号
	 */
	@Column(name="sort_order")
	private Integer sortOrder;

	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	@Override
	public String toString() {
		return "DictDeviceType [name=" + name + ", sortOrder=" + sortOrder + ", getId()=" + getId() + "]";
	}	
	
	

}
