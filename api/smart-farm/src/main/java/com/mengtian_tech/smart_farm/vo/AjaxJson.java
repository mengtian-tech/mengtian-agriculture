package com.mengtian_tech.smart_farm.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonView;

public class AjaxJson {
	@JsonView(JsonViews.SummaryView.class)
	private String msg;
	/**
	 * 注意，这个属性为true，代表服务器会话过时，客户端会自动进入登录页面
	 */
	@JsonView(JsonViews.SummaryView.class)
	private boolean login = false;
	
	@JsonView(JsonViews.SummaryView.class)
	private boolean success = true;
	
	@JsonView(JsonViews.SummaryView.class)
	@SuppressWarnings("rawtypes")
	private List<?> data = new ArrayList();
	/**
	 * 总数
	 */
	@JsonView(JsonViews.SummaryView.class)
	private Long total = 0l;
	
	/**
	 * 当前页号
	 */
	@JsonView(JsonViews.SummaryView.class)
	private Integer current = 0;
	
	@JsonView(JsonViews.SummaryView.class)
	private Integer pageSize = 10;
	
	@JsonView(JsonViews.SummaryView.class)
	private Map<String,Object> attrs = new HashMap<String, Object>();
	
	@JsonView(JsonViews.SummaryView.class)
	private Object object;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public boolean isLogin() {
		return login;
	}

	public void setLogin(boolean login) {
		this.login = login;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Integer getCurrent() {
		return current;
	}

	public void setCurrent(Integer current) {
		this.current = current;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Map<String, Object> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, Object> attrs) {
		this.attrs = attrs;
	}
	
	public static AjaxJson getOk(String msg) {
		AjaxJson json = new AjaxJson();
		json.setSuccess(true);
		json.setMsg(msg);
		return json;
	}
	
	public static AjaxJson getOk() {
		AjaxJson json = new AjaxJson();
		json.setSuccess(true);
		return json;
	}
	/**
	 * 返回操作成功的结果
	 * @param data
	 * @return
	 */
	public static AjaxJson getOk(List<?> data) {
		AjaxJson json = new AjaxJson();
		json.setData(data);
		json.setSuccess(true);
		return json;
	}
	
	/**
	 * 返回操作成功的结果
	 * @param data
	 */
	public static AjaxJson getOk(Object object) {
		AjaxJson json = new AjaxJson();
		json.setObject(object);
		json.setSuccess(true);
		return json;
	}
	
	public static AjaxJson getError(String msg) {
		AjaxJson json = new AjaxJson();
		json.setSuccess(false);
		json.setMsg(msg);
		return json;
	}
	
	/**
	 * 返回操作成功的结果
	 * @param data
	 */
	public static AjaxJson getOk(PageResult<?> data) {
		AjaxJson json = new AjaxJson();
		json.setData(data.getData());
		json.setPageSize(data.getPageSize());
		json.setCurrent(data.getPageNo());
		json.setTotal(data.getTotalCount());
		json.setSuccess(true);
		return json;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
	
	
	
	
}
