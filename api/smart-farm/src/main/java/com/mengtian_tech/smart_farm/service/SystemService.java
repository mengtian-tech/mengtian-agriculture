package com.mengtian_tech.smart_farm.service;

import java.util.List;

import com.mengtian_tech.smart_farm.model.BaseModel;
import com.mengtian_tech.smart_farm.model.SysUser;
import com.mengtian_tech.smart_farm.vo.PageResult;
import com.mengtian_tech.smart_farm.vo.Query;

/**
 * 基础服务接口
 * @author wxd56
 *
 */
public interface SystemService {
	
	/**
	 * 保存
	 * @param model
	 */
	BaseModel save(BaseModel model);
	
	/**
	 * 删除
	 * @param model
	 */
	void delete(BaseModel model);
	
	/**
	 * 根据id获取
	 * @param id
	 */
	<T> T findById(String id,Class<T> clazz);
	
	/**
	 * 查询列表
	 * @param clazz
	 */
	<T>  List<T> findList(Query<T> query);
	
	/**
	 * 查询分页列表
	 * @param clazz
	 */
	<T>  PageResult<T> findPageList(Query<T> query);

	/**
	 * 是否存在查询的结果
	 */
	boolean exist(Query<?> q);
	
}
