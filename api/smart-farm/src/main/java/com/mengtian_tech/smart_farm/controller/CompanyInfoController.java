package com.mengtian_tech.smart_farm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mengtian_tech.smart_farm.model.CompanyInfo;
import com.mengtian_tech.smart_farm.service.SystemService;
import com.mengtian_tech.smart_farm.vo.AjaxJson;
import com.mengtian_tech.smart_farm.vo.PageResult;
import com.mengtian_tech.smart_farm.vo.Query;
/**
 * 企业信息控制器
 * @author wxd56
 */
@Controller
@RequestMapping("szda/company")
public class CompanyInfoController {

	@Autowired
	SystemService systemService;
	
	@RequestMapping("list")
	@ResponseBody
	public AjaxJson list(@RequestBody(required = false) CompanyInfo companyInfo) {
		
		if(companyInfo == null) companyInfo = new CompanyInfo();
		Query<CompanyInfo> q = Query.create(companyInfo);
		PageResult<CompanyInfo> result =  this.systemService.findPageList(q);		
		
		AjaxJson json = AjaxJson.getOk(result);
		return json;
	}
	
	/**
	 * 保存
	 * @param companyInfo
	 */
	@RequestMapping("save")
	@ResponseBody
	public AjaxJson save(@RequestBody CompanyInfo companyInfo) {
		
		this.systemService.save(companyInfo);
		AjaxJson json = AjaxJson.getOk("保存成功");
		return json;
	}
	
	
	/**
	 * 删除
	 */
	@RequestMapping("delete")
	@ResponseBody
	public AjaxJson delete(@RequestBody CompanyInfo companyInfo) {
		this.systemService.delete(companyInfo);
		AjaxJson json = AjaxJson.getOk("保存成功");
		return json;
	}
	
	
}
