package com.mengtian_tech.smart_farm.controller.dict;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mengtian_tech.smart_farm.model.DictPartitionSubType;
import com.mengtian_tech.smart_farm.service.SystemService;
import com.mengtian_tech.smart_farm.vo.AjaxJson;
import com.mengtian_tech.smart_farm.vo.Query;
import com.mengtian_tech.smart_farm.vo.Sorts;

/**
 * 分区子类型控制器
 * @author wxd56
 *
 */
@Controller
@RequestMapping("/dict/partitionSubType")
public class DictPartitionSubTypeController {
	
	@Autowired
	SystemService systemService;
	
	/**
	 * 获取字典列表
	 */
	@RequestMapping("list")
	@ResponseBody
	public AjaxJson partitionTypes(@RequestParam String typeId) {
		
		Query<DictPartitionSubType> q = Query.create(DictPartitionSubType.class);
		q.eq("parent.id",typeId);
		q.addOrder("sortOrder",Sorts.ASC);
		
		List<DictPartitionSubType> list = systemService.findList(q);
		
		return AjaxJson.getOk(list); 
	}
	
	 
}
