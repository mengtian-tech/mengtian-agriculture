package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;

/**
 * 企业信息
 * @author wxd56
 *
 */
@Entity
@Table(name="szda_company_info")
public class CompanyInfo extends BaseModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6724583591025146181L;
	
	/**
	 * 企业名称
	 */
	@JsonView(JsonViews.SummaryView.class)
	@Column(name="name",length = 100)
	private String name;
	/**
	 * 联系人
	 */
	@Column(name="contact_user",length = 20)
	private String contactUser;
	/**
	 * 联系电话
	 */
	@Column(name="contact_phone",length = 20)
	private String contactPhone;
	
	/**
	 * 公司地址
	 */
	@Column(name="address",length = 200)
	private String address;
	
	@Column(name="url",length = 200)
	private String url;
	
	@Column(name="remark",length = 200)
	private String remark;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContactUser() {
		return contactUser;
	}

	public void setContactUser(String contactUser) {
		this.contactUser = contactUser;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "CompanyInfo [name=" + name + ", contactUser=" + contactUser + ", contactPhone=" + contactPhone
				+ ", address=" + address + ", url=" + url + ", remark=" + remark + "]";
	}

	
}
