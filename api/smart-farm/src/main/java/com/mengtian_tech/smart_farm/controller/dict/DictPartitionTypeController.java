package com.mengtian_tech.smart_farm.controller.dict;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mengtian_tech.smart_farm.model.DictPartitionType;
import com.mengtian_tech.smart_farm.service.SystemService;
import com.mengtian_tech.smart_farm.vo.AjaxJson;
import com.mengtian_tech.smart_farm.vo.Query;
import com.mengtian_tech.smart_farm.vo.Sorts;

@Controller
@RequestMapping("/dict/partitionType")
public class DictPartitionTypeController {
	
	@Autowired
	SystemService systemService;
	
	/**
	 * 获取字典列表
	 */
	@RequestMapping("list")
	@ResponseBody
	public AjaxJson partitionTypes() {
		
		Query<DictPartitionType> q = Query.create(DictPartitionType.class);
		q.addOrder("sortOrder",Sorts.ASC);
		
		List<DictPartitionType> list = systemService.findList(q);
		
		return AjaxJson.getOk(list); 
	}
	
	 
}
