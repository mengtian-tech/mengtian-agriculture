package com.mengtian_tech.smart_farm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.model.SysDepart;
import com.mengtian_tech.smart_farm.service.SystemService;
import com.mengtian_tech.smart_farm.vo.AjaxJson;
import com.mengtian_tech.smart_farm.vo.JsonViews;
import com.mengtian_tech.smart_farm.vo.Query;
/**
 * 部门信息控制器
 */
@Controller
@RequestMapping("xtgl/depart")
public class SysDepartController {

	@Autowired
	SystemService systemService;
	
	@JsonView(JsonViews.DepartList.class)
	@RequestMapping("list")
	@ResponseBody
	public AjaxJson list(@RequestBody(required = false) SysDepart companyInfo) {
		
		if(companyInfo == null) companyInfo = new SysDepart();
		Query<SysDepart> q = Query.create(companyInfo);
		q.isNull("parent");
		List<SysDepart> nodeList =  this.systemService.findList(q);		
		
		for(SysDepart node :nodeList) {
			fetchChildren(node);
		}
		
		AjaxJson json = AjaxJson.getOk(nodeList);
		return json;
	}
	
	private void fetchChildren(SysDepart parent) {
		Query<SysDepart>  q = Query.create(SysDepart.class);
		q.eq("parent.id", parent.getId());
		List<SysDepart> children =  this.systemService.findList(q);
		
		if(children.size() > 0) {
			parent.setChildren(children);
			for(SysDepart child: children) {
				fetchChildren(child);
			}
		}
		
	}
	
	/**
	 * 保存
	 * @param companyInfo
	 */
	@RequestMapping("save")
	@ResponseBody
	public AjaxJson save(@RequestBody SysDepart depart) {
		
		this.systemService.save(depart);
		AjaxJson json = AjaxJson.getOk("保存成功");
		return json;
	}
	
	
	/**
	 * 删除
	 */
	@RequestMapping("delete")
	@ResponseBody
	public AjaxJson delete(@RequestBody SysDepart companyInfo) {
		this.systemService.delete(companyInfo);
		AjaxJson json = AjaxJson.getOk("删除成功");
		return json;
	}
	
	
}
