package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 设施信息
 * @author wxd56
 *
 */
@Entity
@Table(name="zygl_facility_info")
public class FacilityInfo extends BaseModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8428728550030802006L;

	/**
	 * 名称
	 */
	@Column(name="name",length = 100)
	private String name;
	 
	@Lob
	@Column(name="coordinate_group")
	private String coordinateGroup;

 
	/**
	 * 备注信息
	 */
	@Column(name="remark",length = 100)
	private String remark;
	 
	
	@ManyToOne
	@JoinColumn(name="base_info_id")
	private BaseInfo baseInfo;


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCoordinateGroup() {
		return coordinateGroup;
	}


	public void setCoordinateGroup(String coordinateGroup) {
		this.coordinateGroup = coordinateGroup;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public BaseInfo getBaseInfo() {
		return baseInfo;
	}


	public void setBaseInfo(BaseInfo baseInfo) {
		this.baseInfo = baseInfo;
	}
	
	
}
