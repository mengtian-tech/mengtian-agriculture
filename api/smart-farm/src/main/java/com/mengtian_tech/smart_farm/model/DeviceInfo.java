package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;

/**
 * 设备信息
 * @author wxd56
 *
 */
@Entity
@Table(name="zygl_device_info")
public class DeviceInfo extends BaseModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -420474306374462L;

	/**
	 * 设备名称
	 */
	@JsonView(JsonViews.SummaryView.class)
	@Column(name="name",length = 100)
	private String name;
	
	 
	
	@Lob
	@Column(name="coordinate_group")
	@JsonView(JsonViews.DeviceList.class)
	private String coordinateGroup;

	 
	
	@ManyToOne
	@JoinColumn(name="base_info_id")
	@JsonView(JsonViews.DeviceList.class)
	private BaseInfo baseInfo;
	
	/**
	 * 设备编号
	 */
	@Column(name="no")
	@JsonView(JsonViews.DeviceList.class)
	private String no;
	
	/**
	 * 设备分类
	 */
	@ManyToOne
	@JoinColumn(name="device_type_id")
	@JsonView(JsonViews.DeviceList.class)
	private DictDeviceType deviceType;
	
	/**
	 * 购买人
	 */
	@Column(name="buy_user_name",length = 100)
	@JsonView(JsonViews.DeviceList.class)
	private String buyUserName;
	
	/**
	 * 购买日期
	 */
	@Column(name="buy_date")
	@JsonView(JsonViews.DeviceList.class)
	private Date buyDate;
	
	/**
	 * 厂商
	 */
	@Column(name="factory",length = 100)
	@JsonView(JsonViews.DeviceList.class)
	private String factory;
	
	/**
	 * 品牌型号
	 */
	@Column(name="brand_model",length = 100)
	@JsonView(JsonViews.DeviceList.class)
	private String brandModel;
	
	/**
	 * 备注信息
	 */
	@Column(name="remark",length = 100)
	@JsonView(JsonViews.DeviceList.class)
	private String remark;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	  
	public String getCoordinateGroup() {
		return coordinateGroup;
	}

	public void setCoordinateGroup(String coordinateGroup) {
		this.coordinateGroup = coordinateGroup;
	}

 

	public BaseInfo getBaseInfo() {
		return baseInfo;
	}

	public void setBaseInfo(BaseInfo baseInfo) {
		this.baseInfo = baseInfo;
	}

	@Override
	public String toString() {
		return "DeviceInfo [name=" + name + ", coordinateGroup=" + coordinateGroup + ", baseInfo=" + baseInfo + ", no="
				+ no + ", deviceType=" + deviceType + ", buyUserName=" + buyUserName + ", buyDate=" + buyDate
				+ ", factory=" + factory + ", brandModel=" + brandModel + ", remark=" + remark + "]";
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public DictDeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DictDeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public String getBuyUserName() {
		return buyUserName;
	}

	public void setBuyUserName(String buyUserName) {
		this.buyUserName = buyUserName;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	public String getFactory() {
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public String getBrandModel() {
		return brandModel;
	}

	public void setBrandModel(String brandModel) {
		this.brandModel = brandModel;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
	
}
