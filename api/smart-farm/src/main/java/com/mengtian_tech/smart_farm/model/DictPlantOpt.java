package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;

/**
 * 农事操作字典
 */
@Entity
@Table(name="dict_plant_opt")
public class DictPlantOpt extends BaseModel implements Serializable{

	private static final long serialVersionUID = 46590837791453L;
	
	/**
	 * 字典名称
	 */
	@Column(name="name",length = 200)
	@JsonView(JsonViews.SummaryView.class)
	private String name;
	
	/**
	 * 排列序号
	 */
	@Column(name="sort_order")
	private Integer sortOrder;
	
	@Column(name="remark",length = 200)
	@JsonView(JsonViews.DictPlantOptList.class)
	private String remark;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}	

}
