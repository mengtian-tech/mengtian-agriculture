package com.mengtian_tech.smart_farm.vo;

public class PageParam {
	private Integer pageSize; 
	private Integer pageNo;
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	@Override
	public String toString() {
		return "PageParam [pageSize=" + pageSize + ", pageNo=" + pageNo + "]";
	}
	
	
}
