package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;

/**
 * 系统用户
 * @author wxd56
 *
 */
@Entity
@Table(name="xtgl_user")
public class SysUser extends BaseModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 930070386520011238L;
	
	/**
	 * 姓名
	 */
	@Column(name="name",length = 100)
	@JsonView(JsonViews.SummaryView.class)
	private String name;
	
	/**
	 * 登录名称
	 */
	@Column(name="login_name",length = 40)
	private String loginName;

	/**
	 * 手机号
	 */
	@Column(name="cellphone",length = 20)
	private String cellphone;
	
	/**
	 * 办公电话
	 */
	@Column(name="office_phone",length = 20)
	private String officePhone;
	
	/**
	 * 电子邮箱
	 */
	@Column(name="email" , length = 100)
	private String email;
	
	/**
	 * 入职时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name="enter_date")
	private Date enterDate;
	
	/**
	 * 部门信息
	 */
	@ManyToOne
	@JoinColumn(name="depart_id")
	private SysDepart depart;
	
	/**
	 * 密码
	 */
	@Column(name="password",length = 50)
	private String password;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getEnterDate() {
		return enterDate;
	}

	public void setEnterDate(Date enterDate) {
		this.enterDate = enterDate;
	}

	public SysDepart getDepart() {
		return depart;
	}

	public void setDepart(SysDepart depart) {
		this.depart = depart;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
