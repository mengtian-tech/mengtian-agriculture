package com.mengtian_tech.smart_farm.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mengtian_tech.smart_farm.model.SysUser;
import com.mengtian_tech.smart_farm.vo.AjaxJson;

/**
 * Sample controller for going to the home page with a message
 */
@Controller
@RequestMapping("/home")
public class HomeController {

	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	
	/**
	 * 获取当前登录的用户
	 */
	@RequestMapping("getCurrentUser")
	@ResponseBody
	public AjaxJson getCurrentUser(HttpSession session) {
		SysUser user =  (SysUser) session.getAttribute("loginUser");
		return AjaxJson.getOk(user);
	}
	

}
