package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;

/**
 * 地块信息
 * @author wxd56
 *
 */
@Entity
@Table(name="zygl_field_info")
public class FieldInfo extends BaseModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4204748610706374462L;

	/**
	 * 地块名称
	 */
	@Column(name="name",length = 100)
	@JsonView(JsonViews.SummaryView.class)
	private String name;
	
	/**
	 * 透明度
	 */
	@Column(name="opacity")
	@JsonView(JsonViews.FieldList.class)
	private Float opacity;
	
	/**
	 * 地块类型
	 */
	@ManyToOne
	@JoinColumn(name="type_id")
	@JsonView(JsonViews.FieldList.class)
	private DictFieldType type;
	
	/**
	 * 负责人
	 */
	@Column(name="fzr",length = 20)
	@JsonView(JsonViews.FieldList.class)
	private String fzr;
	
	/**
	 * 联系电话
	 */
	@Column(name="phone",length = 30)
	@JsonView(JsonViews.FieldList.class)
	private String phone;
	
	/**
	 * 面积
	 */
	@Column(name="mu_number")
	@JsonView(JsonViews.FieldList.class)
	private Float muNumber;
	
	@Lob
	@Column(name="coordinate_group")
	@JsonView(JsonViews.FieldList.class)
	private String coordinateGroup;

	@Column(name="view_color",length = 30)
	@JsonView(JsonViews.FieldList.class)
	private String viewColor;
	
	/**
	 * 年产量
	 */
	@Column(name="year_production")
	@JsonView(JsonViews.FieldList.class)
	private Float yearProdution;
	
	@ManyToOne
	@JoinColumn(name="base_info_id")
	@JsonView(JsonViews.FieldList.class)
	private BaseInfo baseInfo;
	
	/**
	 * 备注信息
	 */
	@Column(name="remark",length = 100)
	@JsonView(JsonViews.FieldList.class)
	private String remark; 

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getOpacity() {
		return opacity;
	}

	public void setOpacity(Float opacity) {
		this.opacity = opacity;
	}

	public DictFieldType getType() {
		return type;
	}

	public void setType(DictFieldType type) {
		this.type = type;
	}

	public String getFzr() {
		return fzr;
	}

	public void setFzr(String fzr) {
		this.fzr = fzr;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Float getMuNumber() {
		return muNumber;
	}

	public void setMuNumber(Float muNumber) {
		this.muNumber = muNumber;
	}

	public String getCoordinateGroup() {
		return coordinateGroup;
	}

	public void setCoordinateGroup(String coordinateGroup) {
		this.coordinateGroup = coordinateGroup;
	}

	public String getViewColor() {
		return viewColor;
	}

	public void setViewColor(String viewColor) {
		this.viewColor = viewColor;
	}

	public Float getYearProdution() {
		return yearProdution;
	}

	public void setYearProdution(Float yearProdution) {
		this.yearProdution = yearProdution;
	}

	public BaseInfo getBaseInfo() {
		return baseInfo;
	}

	public void setBaseInfo(BaseInfo baseInfo) {
		this.baseInfo = baseInfo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
	
}
