package com.mengtian_tech.smart_farm.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mengtian_tech.smart_farm.model.FacilityInfo;
import com.mengtian_tech.smart_farm.service.SystemService;
import com.mengtian_tech.smart_farm.vo.AjaxJson;
import com.mengtian_tech.smart_farm.vo.PageResult;
import com.mengtian_tech.smart_farm.vo.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * 设施控制器
 * @author wxd56
 *
 */
@Controller
@RequestMapping("/zygl/szhgl/facility")
public class FacilityController {

	@Autowired
	SystemService systemService;
	
	@RequestMapping("list")
	@ResponseBody
	public AjaxJson list( @RequestBody(required = false) FacilityInfo obj) {
		if(obj == null) obj = new FacilityInfo();
		Query<FacilityInfo> q = Query.create(obj);
		PageResult<FacilityInfo> result =  this.systemService.findPageList(q);		
		AjaxJson json = AjaxJson.getOk(result);
		return json;
	}
	
	/**
	 * 保存
	 */
	@RequestMapping("save")
	@ResponseBody
	public AjaxJson save(@RequestBody FacilityInfo baseInfo) {
		this.systemService.save(baseInfo);				
		AjaxJson json =  AjaxJson.getOk("保存成功");
		json.setObject(baseInfo);
		return json;
	}
	
	/**
	 * 删除信息
	 */
	@RequestMapping("delete")
	@ResponseBody
	public AjaxJson delete(@RequestBody FacilityInfo baseInfo) {
		this.systemService.delete(baseInfo);				
		AjaxJson json =  AjaxJson.getOk("删除成功");
		json.setObject(baseInfo);
		return json;
	}
	
}
