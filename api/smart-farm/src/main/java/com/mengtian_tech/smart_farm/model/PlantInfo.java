package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;

/**
 * 种植品种库信息
 * @author wxd56
 *
 */
@Entity
@Table(name="zygl_plant_info")
public class PlantInfo extends BaseModel implements Serializable{

	private static final long serialVersionUID = -5770657144140715693L;
	
	/**
	 * 品种名称
	 */
	@Column(name="name",length = 200)
	@JsonView(JsonViews.SummaryView.class)
	private String name;
	
	/**
	 * 上级品种库
	 */
	@ManyToOne
	@JoinColumn(name="parent_id")
	@JsonView(JsonViews.SummaryView.class)
	private PlantInfo parent;
	
	
	@Transient
	@JsonView(JsonViews.PlantTreeView.class)
	private List<PlantInfo> children = null;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PlantInfo getParent() {
		return parent;
	}

	public void setParent(PlantInfo parent) {
		this.parent = parent;
	}

	public List<PlantInfo> getChildren() {
		return children;
	}

	public void setChildren(List<PlantInfo> children) {
		this.children = children;
	}
	
	

}
