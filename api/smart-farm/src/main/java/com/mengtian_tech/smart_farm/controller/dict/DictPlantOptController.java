package com.mengtian_tech.smart_farm.controller.dict;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.model.DictPlantOpt;
import com.mengtian_tech.smart_farm.service.SystemService;
import com.mengtian_tech.smart_farm.vo.AjaxJson;
import com.mengtian_tech.smart_farm.vo.JsonViews;
import com.mengtian_tech.smart_farm.vo.Query;
import com.mengtian_tech.smart_farm.vo.Sorts;

/**
 * 种植操作字典
 *
 */
@Controller
@RequestMapping("/dict/plantOpt")
public class DictPlantOptController {
	
	@Autowired
	SystemService systemService;
	
	/**
	 * 获取字典列表
	 */
	@RequestMapping("list")
	@ResponseBody
	@JsonView(JsonViews.DictPlantOptList.class)
	public AjaxJson partitionTypes(@RequestBody(required = false) DictPlantOpt opt) {
		if(opt == null) opt = new DictPlantOpt();
		Query<DictPlantOpt> q = Query.create(opt);
		q.addOrder("sortOrder",Sorts.ASC);
		
		List<DictPlantOpt> list = systemService.findList(q);
		
		return AjaxJson.getOk(list); 
	}
	
	
	/**
	 * 保存
	 */
	@RequestMapping("save")
	@ResponseBody
	public AjaxJson save(@RequestBody DictPlantOpt depart) {
		
		this.systemService.save(depart);
		AjaxJson json = AjaxJson.getOk("保存成功");
		return json;
	}
	
	
	/**
	 * 删除
	 */
	@RequestMapping("delete")
	@ResponseBody
	public AjaxJson delete(@RequestBody DictPlantOpt companyInfo) {
		this.systemService.delete(companyInfo);
		AjaxJson json = AjaxJson.getOk("删除成功");
		return json;
	}
	 
}
