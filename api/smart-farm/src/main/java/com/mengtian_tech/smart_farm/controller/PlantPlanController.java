package com.mengtian_tech.smart_farm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.model.PlantPlan;
import com.mengtian_tech.smart_farm.service.SystemService;
import com.mengtian_tech.smart_farm.vo.AjaxJson;
import com.mengtian_tech.smart_farm.vo.JsonViews;
import com.mengtian_tech.smart_farm.vo.Query;
/**
 * 生产计划控制器
 */
@Controller
@RequestMapping("scgl/plan")
public class PlantPlanController {

	@Autowired
	SystemService systemService;
	
	@JsonView(JsonViews.PlanList.class)
	@RequestMapping("list")
	@ResponseBody
	public AjaxJson list(@RequestBody(required = false) PlantPlan companyInfo) {
		
		if(companyInfo == null) companyInfo = new PlantPlan();
		Query<PlantPlan> q = Query.create(companyInfo);
		List<PlantPlan> nodeList =  this.systemService.findList(q);		
		AjaxJson json = AjaxJson.getOk(nodeList);
		return json;
	}
	
	 
	/**
	 * 保存
	 * @param companyInfo
	 */
	@RequestMapping("save")
	@ResponseBody
	public AjaxJson save(@RequestBody PlantPlan companyInfo) {
		
		this.systemService.save(companyInfo);
		AjaxJson json = AjaxJson.getOk("保存成功");
		return json;
	}
	
	
	/**
	 * 删除
	 */
	@RequestMapping("delete")
	@ResponseBody
	public AjaxJson delete(@RequestBody PlantPlan companyInfo) {
		this.systemService.delete(companyInfo);
		AjaxJson json = AjaxJson.getOk("保存成功");
		return json;
	}
	
	
}
