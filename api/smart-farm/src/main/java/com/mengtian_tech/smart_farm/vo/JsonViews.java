package com.mengtian_tech.smart_farm.vo;

/**
 * 统一定义Json的视图返回
 * @author wxd56
 */
public class JsonViews {
	/**
	 * 概要信息的Json视图，后面所有的视图都应该继承当前视图
	 * Model根据自己的情况，只把主要字段标记上即可
	 * @author wxd56
	 */
	public interface SummaryView{}
	
	/**
	 * 分区列表视图
	 * @author wxd56
	 */
	public interface PartitionView extends SummaryView{}
	
	/**
	 * 地块列表视图
	 * @author wxd56
	 */
	public interface FieldList extends SummaryView{}
	
	/**
	 * 种植库
	 * @author wxd56
	 */
	public interface PlantTreeView extends SummaryView{}

	/**
	 * 生产计划
	 * @author wxd56
	 *
	 */
	public interface PlanList extends SummaryView{}
	
	/**
	 * 部门列表视图
	 * @author wxd56
	 *
	 */
	public interface DepartList extends SummaryView{}
	
	/**
	 * 农事操作字典列表视图
	 * @author wxd56
	 *
	 */
	public interface DictPlantOptList extends SummaryView{}
	

	/**
	 * 农事操作列表
	 */
	public interface FarmWorkList extends SummaryView{}
	
	/**
	 * 设备列表
	 */
	public interface DeviceList extends SummaryView{}
}
