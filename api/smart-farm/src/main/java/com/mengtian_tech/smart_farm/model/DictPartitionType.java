package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;

/**
 * 分区类型
 * @author wxd56
 *
 */
@Entity
@Table(name="dict_partition_type")
public class DictPartitionType extends BaseModel implements Serializable{

	private static final long serialVersionUID = 465108131127791453L;
	
	/**
	 * 类型名称
	 */
	@Column(name="name",length = 200)
	@JsonView(JsonViews.SummaryView.class)
	private String name;
	
	/**
	 * 排列序号
	 */
	@Column(name="sort_order")
	private Integer sortOrder;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}	

}
