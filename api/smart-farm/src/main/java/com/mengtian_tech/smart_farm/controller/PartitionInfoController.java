package com.mengtian_tech.smart_farm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.model.PartitionInfo;
import com.mengtian_tech.smart_farm.service.SystemService;
import com.mengtian_tech.smart_farm.vo.AjaxJson;
import com.mengtian_tech.smart_farm.vo.JsonViews;
import com.mengtian_tech.smart_farm.vo.PageResult;
import com.mengtian_tech.smart_farm.vo.Query;

/**
 * 分区信息控制器
 * @author wxd56
 */
@Controller
@RequestMapping("/zygl/szhgl/partition")
public class PartitionInfoController {

	@Autowired
	SystemService systemService;
	
	@RequestMapping("list")
	@ResponseBody
	@JsonView(JsonViews.PartitionView.class)
	public AjaxJson list( @RequestBody(required = false) PartitionInfo obj) {
		if(obj == null) obj = new PartitionInfo();
		Query<PartitionInfo> q = Query.create(obj);
		PageResult<PartitionInfo> result =  this.systemService.findPageList(q);		
		
		AjaxJson json = AjaxJson.getOk(result);
		return json;
	}
	
	/**
	 * 保存
	 */
	@RequestMapping("save")
	@ResponseBody
	public AjaxJson save(@RequestBody PartitionInfo baseInfo) {
		this.systemService.save(baseInfo);				
		AjaxJson json =  AjaxJson.getOk("保存成功");
		json.setObject(baseInfo);
		return json;
	}
	
	/**
	 * 删除信息
	 */
	@RequestMapping("delete")
	@ResponseBody
	public AjaxJson delete(@RequestBody PartitionInfo baseInfo) {
		this.systemService.delete(baseInfo);				
		AjaxJson json =  AjaxJson.getOk("删除成功");
		json.setObject(baseInfo);
		return json;
	}
	
}
