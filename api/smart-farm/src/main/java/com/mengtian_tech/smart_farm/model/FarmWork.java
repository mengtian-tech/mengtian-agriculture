package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;

/**
 * 农事信息
 * @author wxd56
 *
 */
@Entity
@Table(name="scgl_farm_work")
public class FarmWork extends BaseModel implements Serializable {
 
	private static final long serialVersionUID = 9073719625761024162L;
	
	/**
	 * 农事名称
	 */
	@Column(name="name",length = 200)
	@JsonView(JsonViews.SummaryView.class)
	private String name;
	
	/**
	 * 农事操作
	 */
	@ManyToOne
	@JoinColumn(name="opt_id")
	@JsonView(JsonViews.FarmWorkList.class)
	private DictPlantOpt opt;
	
	/**
	 * 负责人
	 */
	@ManyToOne
	@JoinColumn(name="user_id")
	@JsonView(JsonViews.FarmWorkList.class)
	private SysUser user;
	
	@Column(name="begin_date")
	@JsonView(JsonViews.FarmWorkList.class)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date beginDate;
	
	@Column(name="end_date")
	@JsonView(JsonViews.FarmWorkList.class)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date endDate;
	
	/**
	 * 状态
	 */
	@Column(name="work_state")
	@JsonView(JsonViews.FarmWorkList.class)
	private Integer state;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DictPlantOpt getOpt() {
		return opt;
	}

	public void setOpt(DictPlantOpt opt) {
		this.opt = opt;
	}

	public SysUser getUser() {
		return user;
	}

	public void setUser(SysUser user) {
		this.user = user;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
	
	

}
