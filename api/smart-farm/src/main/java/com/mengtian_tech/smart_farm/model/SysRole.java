package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;

@Entity
@Table(name="xtgl_role")
public class SysRole extends BaseModel implements Serializable{

	private static final long serialVersionUID = -2397678136347418808L;
	
	/**
	 * 名称
	 */
	@Column(name="name",length = 100)
	@JsonView(JsonViews.SummaryView.class)
	private String name;
	
	/**
	 * 备注
	 */
	@Column(name="remark",length = 100)
	@JsonView(JsonViews.SummaryView.class)
	private String remark;
	
	@Column(name="sort_order")
	@JsonView(JsonViews.SummaryView.class)
	private Integer sortOrder;
	
	@JsonView(JsonViews.SummaryView.class)
	@Column(name="role_state")
	private Integer state;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
	
}
