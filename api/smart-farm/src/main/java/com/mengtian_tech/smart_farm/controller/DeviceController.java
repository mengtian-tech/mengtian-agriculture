package com.mengtian_tech.smart_farm.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.model.DeviceInfo;
import com.mengtian_tech.smart_farm.service.SystemService;
import com.mengtian_tech.smart_farm.vo.AjaxJson;
import com.mengtian_tech.smart_farm.vo.JsonViews;
import com.mengtian_tech.smart_farm.vo.PageResult;
import com.mengtian_tech.smart_farm.vo.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * 物联网设备控制器
 *
 */
@Controller
@RequestMapping("/zygl/szhgl/device")
public class DeviceController {

	@Autowired
	SystemService systemService;
	
	@RequestMapping("list")
	@ResponseBody
	@JsonView(JsonViews.DeviceList.class)
	public AjaxJson list(@RequestBody(required = false) DeviceInfo obj) {
		if(obj == null) obj = new DeviceInfo();
		Query<DeviceInfo> q = Query.create(obj);
		PageResult<DeviceInfo> result =  this.systemService.findPageList(q);		
		AjaxJson json = AjaxJson.getOk(result);
		return json;
	}
	
	/**
	 * 保存
	 */
	@RequestMapping("save")
	@ResponseBody
	public AjaxJson save(@RequestBody DeviceInfo deviceInfo) {
		System.out.println("device info");
		System.out.println(deviceInfo);
		this.systemService.save(deviceInfo);				
		AjaxJson json =  AjaxJson.getOk("保存成功");
		json.setObject(deviceInfo);
		return json;
	}
	
	/**
	 * 删除信息
	 */
	@RequestMapping("delete")
	@ResponseBody
	public AjaxJson delete(@RequestBody DeviceInfo baseInfo) {
		this.systemService.delete(baseInfo);				
		AjaxJson json =  AjaxJson.getOk("删除成功");
		json.setObject(baseInfo);
		return json;
	}
	
	
}
