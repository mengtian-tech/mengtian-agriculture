package com.mengtian_tech.smart_farm.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.model.DailyCheck;
import com.mengtian_tech.smart_farm.service.SystemService;
import com.mengtian_tech.smart_farm.vo.AjaxJson;
import com.mengtian_tech.smart_farm.vo.JsonViews;
import com.mengtian_tech.smart_farm.vo.PageResult;
import com.mengtian_tech.smart_farm.vo.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *日常巡检控制器
 */
@Controller
@RequestMapping("/scgl/dailyCheck")
public class DailyCheckController {

	@Autowired
	SystemService systemService;
	
	@RequestMapping("list")
	@ResponseBody
	@JsonView(JsonViews.SummaryView.class)
	public AjaxJson list(@RequestBody(required = false) DailyCheck obj) {
		if(obj == null) obj = new DailyCheck();
		Query<DailyCheck> q = Query.create(obj);
		PageResult<DailyCheck> result =  this.systemService.findPageList(q);		
		AjaxJson json = AjaxJson.getOk(result);
		return json;
	}
	
	/**
	 * 保存
	 */
	@RequestMapping("save")
	@ResponseBody
	public AjaxJson save(@RequestBody DailyCheck check) {
		this.systemService.save(check);				
		AjaxJson json =  AjaxJson.getOk("保存成功");
		json.setObject(check);
		return json;
	}
	
	/**
	 * 删除基地信息
	 */
	@RequestMapping("delete")
	@ResponseBody
	public AjaxJson delete(@RequestBody DailyCheck check) {
		this.systemService.delete(check);				
		AjaxJson json =  AjaxJson.getOk("删除成功");
		json.setObject(check);
		return json;
	}
}
