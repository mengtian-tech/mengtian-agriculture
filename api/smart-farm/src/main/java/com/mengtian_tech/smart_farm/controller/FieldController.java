package com.mengtian_tech.smart_farm.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.model.FieldInfo;
import com.mengtian_tech.smart_farm.service.SystemService;
import com.mengtian_tech.smart_farm.vo.AjaxJson;
import com.mengtian_tech.smart_farm.vo.JsonViews;
import com.mengtian_tech.smart_farm.vo.PageResult;
import com.mengtian_tech.smart_farm.vo.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * 地块信息控制器
 * @author wxd56
 *
 */
@Controller
@RequestMapping("/zygl/szhgl/field")
public class FieldController {

	@Autowired
	SystemService systemService;
	
	@RequestMapping("list")
	@ResponseBody
	@JsonView(JsonViews.FieldList.class)
	public AjaxJson list(@RequestBody(required = false) FieldInfo obj) {
		if(obj == null) obj = new FieldInfo();
		Query<FieldInfo> q = Query.create(obj);
		PageResult<FieldInfo> result =  this.systemService.findPageList(q);		
		AjaxJson json = AjaxJson.getOk(result);
		return json;
	}
	
	/**
	 * 保存
	 */
	@RequestMapping("save")
	@ResponseBody
	public AjaxJson save(@RequestBody FieldInfo baseInfo) {
		this.systemService.save(baseInfo);				
		AjaxJson json =  AjaxJson.getOk("保存成功");
		json.setObject(baseInfo);
		return json;
	}
	
	/**
	 * 删除基地信息
	 */
	@RequestMapping("delete")
	@ResponseBody
	public AjaxJson delete(@RequestBody FieldInfo baseInfo) {
		this.systemService.delete(baseInfo);				
		AjaxJson json =  AjaxJson.getOk("删除成功");
		json.setObject(baseInfo);
		return json;
	}
}
