package com.mengtian_tech.smart_farm.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mengtian_tech.smart_farm.model.SysUser;
import com.mengtian_tech.smart_farm.service.SystemService;
import com.mengtian_tech.smart_farm.vo.AjaxJson;
import com.mengtian_tech.smart_farm.vo.Query;

@Controller
@RequestMapping("login")
public class LoginController {
	
	@Autowired
	SystemService systemService;

	@RequestMapping("login")
	@ResponseBody
	public AjaxJson index(@RequestBody SysUser user , HttpSession session) {
		Query<SysUser> q = Query.create(SysUser.class);
		q.eq("loginName", user.getLoginName());
		q.eq("password", user.getPassword());
		
		List<SysUser> list = systemService.findList(q);
		if(list.size() == 0) {
			return AjaxJson.getError("用户名或密码错误");
		}
		
		SysUser loginUser = list.get(0);
		session.setAttribute("loginUser", loginUser);
				
		return AjaxJson.getOk("登录成功");
	}
	
	
	
	
	@RequestMapping("logout")
	@ResponseBody
	public AjaxJson logout( HttpSession session) {		 
		session.setAttribute("loginUser", null);				
		return AjaxJson.getOk("注销成功");
	}
}
