package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;

/**
 * 分区信息
 * @author wxd56
 */
@Entity
@Table(name="zygl_partition_info")
public class PartitionInfo extends BaseModel implements Serializable{

	private static final long serialVersionUID = -3172260882853886862L;
	
	/**
	 * 分区名称
	 */
	@Column(name="name",length = 120)
	@JsonView(JsonViews.SummaryView.class)
	private String name;
	/**
	 * 透明度
	 */
	@Column(name="opacity")
	@JsonView(JsonViews.SummaryView.class)
	private Float opacity;
	
	/**
	 * 分区类型
	 */
	@ManyToOne
	@JoinColumn(name="type_id")
	@JsonView(JsonViews.SummaryView.class)
	private DictPartitionType type;
	
	/**
	 * 子类型
	 */
	@ManyToOne
	@JoinColumn(name="sub_type_id")
	@JsonView(JsonViews.SummaryView.class)
	private DictPartitionSubType subType;
	
	/**
	 * 亩数
	 */
	@Column(name="mu_number")
	@JsonView(JsonViews.SummaryView.class)
	private BigDecimal muNumber;
	
	/**
	 * 坐标集合
	 */
	@Lob
	@Column(name="coordinate_group")
	@JsonView(JsonViews.PartitionView.class)
	private String coordinateGroup;
	
	/**
	 * 区域颜色
	 */
	@JsonView(JsonViews.PartitionView.class)
	@Column(name="view_color",length = 50)
	private String viewColor;
	
	/**
	 * 基地信息
	 */
	@ManyToOne
	@JoinColumn(name="base_id")
	@JsonView(JsonViews.SummaryView.class)
	private BaseInfo baseInfo;
	
	/**
	 * 备注信息
	 */
	@Column(name="remark",length = 100)
	@JsonView(JsonViews.PartitionView.class)
	private String remark; 

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getOpacity() {
		return opacity;
	}

	public void setOpacity(Float opacity) {
		this.opacity = opacity;
	}

	public DictPartitionType getType() {
		return type;
	}

	public void setType(DictPartitionType type) {
		this.type = type;
	}

	public DictPartitionSubType getSubType() {
		return subType;
	}

	public void setSubType(DictPartitionSubType subType) {
		this.subType = subType;
	}

	public BigDecimal getMuNumber() {
		return muNumber;
	}

	public void setMuNumber(BigDecimal muNumber) {
		this.muNumber = muNumber;
	}

	public String getCoordinateGroup() {
		return coordinateGroup;
	}

	public void setCoordinateGroup(String coordinateGroup) {
		this.coordinateGroup = coordinateGroup;
	}

	public String getViewColor() {
		return viewColor;
	}

	public void setViewColor(String viewColor) {
		this.viewColor = viewColor;
	}

	public BaseInfo getBaseInfo() {
		return baseInfo;
	}

	public void setBaseInfo(BaseInfo baseInfo) {
		this.baseInfo = baseInfo;
	}

	@Override
	public String toString() {
		return "PartitionInfo [name=" + name + ", opacity=" + opacity + ", type=" + type + ", subType=" + subType
				+ ", muNumber=" + muNumber + ", coordinateGroup=" + coordinateGroup + ", viewColor=" + viewColor
				+ ", baseInfo=" + baseInfo + "]";
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	

}
