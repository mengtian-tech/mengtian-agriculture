package com.mengtian_tech.smart_farm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonView;
import com.mengtian_tech.smart_farm.vo.JsonViews;

/**
 * 部门信息
 * @author wxd56
 *
 */
@Entity
@Table(name="xtgl_depart")
public class SysDepart extends BaseModel implements Serializable{

	private static final long serialVersionUID = -769158862259636249L;
	
	/**
	 * 部门名称
	 */
	@Column(name="name",length = 100)
	@JsonView(JsonViews.SummaryView.class)
	private String name;

	/**
	 * 上级部门
	 */
	@ManyToOne
	@JoinColumn(name="parent_id")
	@JsonView(JsonViews.SummaryView.class)
	private SysDepart parent;
	
	@Transient
	@JsonView(JsonViews.SummaryView.class)
	private List<SysDepart> children;
	
	/**
	 * 备注信息
	 */
	@Column(name="remark",length = 100)
	@JsonView(JsonViews.DepartList.class)
	private String remark;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SysDepart> getChildren() {
		return children;
	}

	public void setChildren(List<SysDepart> children) {
		this.children = children;
	}

	public SysDepart getParent() {
		return parent;
	}

	public void setParent(SysDepart parent) {
		this.parent = parent;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	

}
