<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
	<title>Home</title>
</head>
<body>
<h1>
	  ${person.id} - ${person.firstName}  ${person.lastName} 
</h1>
<form action="/smart-farm/api/person/edit" method="post" enctype="application/x-www-form-urlencoded">
    ID - ${person.id}<br/>
    <p>
        First Name<br/>
        <input name="firstName"  type="text" />
    </p>
    <p>
        Last Name<br/>
        <input name="lastName" />
    </p>
    <input type="submit" value="提交"/>
</form>

</body>
</html>
