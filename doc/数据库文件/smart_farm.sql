/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : smart_farm

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2021-12-07 17:43:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dict_device_type
-- ----------------------------
DROP TABLE IF EXISTS `dict_device_type`;
CREATE TABLE `dict_device_type` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dict_device_type
-- ----------------------------
INSERT INTO `dict_device_type` VALUES ('1', null, '0', null, '视频监控', '1');
INSERT INTO `dict_device_type` VALUES ('2', null, '0', null, '传感器', '2');
INSERT INTO `dict_device_type` VALUES ('3', null, '0', null, '其他', '3');

-- ----------------------------
-- Table structure for dict_field_type
-- ----------------------------
DROP TABLE IF EXISTS `dict_field_type`;
CREATE TABLE `dict_field_type` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dict_field_type
-- ----------------------------
INSERT INTO `dict_field_type` VALUES ('1', '2021-11-24 09:36:38', '0', '2021-11-24 09:36:41', '农田', '1');
INSERT INTO `dict_field_type` VALUES ('2', null, '0', null, '开荒地', '2');
INSERT INTO `dict_field_type` VALUES ('3', null, '0', null, '草场地', '3');
INSERT INTO `dict_field_type` VALUES ('4', null, '0', null, '温室', '4');
INSERT INTO `dict_field_type` VALUES ('5', null, '0', null, '春秋棚', '5');
INSERT INTO `dict_field_type` VALUES ('6', null, '0', null, '水面', '6');

-- ----------------------------
-- Table structure for dict_partition_sub_type
-- ----------------------------
DROP TABLE IF EXISTS `dict_partition_sub_type`;
CREATE TABLE `dict_partition_sub_type` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `parent_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKF8DDFAB7A7FA8070` (`parent_id`),
  CONSTRAINT `FKF8DDFAB7A7FA8070` FOREIGN KEY (`parent_id`) REFERENCES `dict_partition_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dict_partition_sub_type
-- ----------------------------
INSERT INTO `dict_partition_sub_type` VALUES ('1', null, '0', null, '大田', null, '1');
INSERT INTO `dict_partition_sub_type` VALUES ('2', null, '0', null, '大棚', null, '1');
INSERT INTO `dict_partition_sub_type` VALUES ('3', null, '0', null, '生产车间', null, '2');
INSERT INTO `dict_partition_sub_type` VALUES ('4', null, '0', null, '连栋温室', null, '2');
INSERT INTO `dict_partition_sub_type` VALUES ('5', null, '0', null, '仓储', null, '3');
INSERT INTO `dict_partition_sub_type` VALUES ('6', null, '0', null, '市场', null, '3');
INSERT INTO `dict_partition_sub_type` VALUES ('7', null, '0', null, '物流', null, '3');

-- ----------------------------
-- Table structure for dict_partition_type
-- ----------------------------
DROP TABLE IF EXISTS `dict_partition_type`;
CREATE TABLE `dict_partition_type` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dict_partition_type
-- ----------------------------
INSERT INTO `dict_partition_type` VALUES ('1', null, '0', null, '种植类', '1');
INSERT INTO `dict_partition_type` VALUES ('2', null, '0', null, '加工类', '2');
INSERT INTO `dict_partition_type` VALUES ('3', null, '0', null, '服务类', '3');

-- ----------------------------
-- Table structure for dict_plant_opt
-- ----------------------------
DROP TABLE IF EXISTS `dict_plant_opt`;
CREATE TABLE `dict_plant_opt` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dict_plant_opt
-- ----------------------------
INSERT INTO `dict_plant_opt` VALUES ('402809817d6987b9017d698939080000', '2021-11-29 10:32:20', '0', '2021-11-29 10:32:20', '施肥', null, '施肥测试');
INSERT INTO `dict_plant_opt` VALUES ('402809817d699092017d6992f8370000', '2021-11-29 10:42:59', '0', '2021-11-29 10:42:59', '打药', null, '');
INSERT INTO `dict_plant_opt` VALUES ('402809817d699092017d699315ee0001', '2021-11-29 10:43:06', '1', '2021-11-29 10:43:09', '3', null, '');
INSERT INTO `dict_plant_opt` VALUES ('402809817d699092017d69936f650002', '2021-11-29 10:43:29', '0', '2021-11-29 10:43:29', '灌溉', null, '');

-- ----------------------------
-- Table structure for person
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of person
-- ----------------------------
INSERT INTO `person` VALUES ('402809817d4522ed017d4524337c0000', null, null, null, '&#29579;', '&#26093;&#19996;');
INSERT INTO `person` VALUES ('402809817d45325a017d45333c730000', null, null, null, '&#29579;&#26093;&#19996;', '3232');
INSERT INTO `person` VALUES ('402809817d456fff017d45716a8c0000', null, null, null, null, null);
INSERT INTO `person` VALUES ('402809817d456fff017d4571e5f50001', null, null, null, null, null);
INSERT INTO `person` VALUES ('402809817d456fff017d457439170002', null, null, null, null, null);
INSERT INTO `person` VALUES ('402809817d45853d017d458555f00000', null, null, null, null, null);
INSERT INTO `person` VALUES ('402809817d45853d017d458674a60001', null, null, null, '范德萨', '');
INSERT INTO `person` VALUES ('402809817d458c5d017d458c8db80000', null, null, null, '我先', '方法');
INSERT INTO `person` VALUES ('402809817d45e97e017d45e9ac980000', null, '0', null, '我先', '方法');
INSERT INTO `person` VALUES ('402809817d45e97e017d45e9ce560001', null, '0', null, '43', '走');
INSERT INTO `person` VALUES ('402809817d45eade017d45eb22a10000', '2021-11-22 12:32:57', '0', '2021-11-22 12:32:57', '王旭东', '走');

-- ----------------------------
-- Table structure for scgl_daily_check
-- ----------------------------
DROP TABLE IF EXISTS `scgl_daily_check`;
CREATE TABLE `scgl_daily_check` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `air_temp` float DEFAULT NULL,
  `check_date` datetime DEFAULT NULL,
  `solid_temp` float DEFAULT NULL,
  `check_state` int(11) DEFAULT NULL,
  `field_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK739C6D585AC257BA` (`field_id`),
  KEY `FK739C6D588BB2C739` (`user_id`),
  CONSTRAINT `FK739C6D585AC257BA` FOREIGN KEY (`field_id`) REFERENCES `zygl_field_info` (`id`),
  CONSTRAINT `FK739C6D588BB2C739` FOREIGN KEY (`user_id`) REFERENCES `xtgl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of scgl_daily_check
-- ----------------------------
INSERT INTO `scgl_daily_check` VALUES ('402809817d8aa4b5017d8aaeb0f60000', '2021-12-05 21:00:44', '0', '2021-12-05 21:00:44', '22', '2021-12-09 08:00:00', '16', '1', '1', '402809817d6693b0017d6695a4780000');
INSERT INTO `scgl_daily_check` VALUES ('402809817d8aa4b5017d8ab0fdaa0001', '2021-12-05 21:03:14', '0', '2021-12-05 21:03:14', '25.2', '2021-12-05 08:00:00', '16', '1', '1', '402809817d6698ca017d669f5c4c0000');

-- ----------------------------
-- Table structure for scgl_farm_work
-- ----------------------------
DROP TABLE IF EXISTS `scgl_farm_work`;
CREATE TABLE `scgl_farm_work` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `begin_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `work_state` int(11) DEFAULT NULL,
  `opt_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1125CF08BB2C739` (`user_id`),
  KEY `FK1125CF0AC05A93` (`opt_id`),
  CONSTRAINT `FK1125CF08BB2C739` FOREIGN KEY (`user_id`) REFERENCES `xtgl_user` (`id`),
  CONSTRAINT `FK1125CF0AC05A93` FOREIGN KEY (`opt_id`) REFERENCES `dict_plant_opt` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of scgl_farm_work
-- ----------------------------
INSERT INTO `scgl_farm_work` VALUES ('402809817d69b4b1017d69b5ce5e0000', '2021-11-29 11:21:02', '0', '2021-11-29 11:21:02', '2021-11-30 08:00:00', null, '施肥任务', '2', '402809817d699092017d6992f8370000', '402809817d6698ca017d669f5c4c0000');
INSERT INTO `scgl_farm_work` VALUES ('402809817d69b65b017d69b748e00000', '2021-11-29 11:22:39', '1', '2021-11-29 11:22:42', null, null, '1232', null, null, null);
INSERT INTO `scgl_farm_work` VALUES ('402809817d6a4f46017d6a5638be0000', '2021-11-29 14:16:15', '0', '2021-11-29 14:16:15', '2021-11-10 08:00:00', null, '灌溉任务', null, '402809817d699092017d69936f650002', '402809817d6698ca017d669f5c4c0000');

-- ----------------------------
-- Table structure for scgl_plant_plan
-- ----------------------------
DROP TABLE IF EXISTS `scgl_plant_plan`;
CREATE TABLE `scgl_plant_plan` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `pick_date` datetime DEFAULT NULL,
  `pre_product` decimal(19,2) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `plant_id` varchar(36) DEFAULT NULL,
  `owner_user_id` varchar(36) DEFAULT NULL,
  `plant_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7C7E9FC73492F59A` (`plant_id`),
  KEY `FK7C7E9FC7785DFDAD` (`owner_user_id`),
  CONSTRAINT `FK7C7E9FC73492F59A` FOREIGN KEY (`plant_id`) REFERENCES `zygl_plant_info` (`id`),
  CONSTRAINT `FK7C7E9FC7785DFDAD` FOREIGN KEY (`owner_user_id`) REFERENCES `xtgl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of scgl_plant_plan
-- ----------------------------
INSERT INTO `scgl_plant_plan` VALUES ('402809817d64ac4b017d64acdc1d0000', '2021-11-28 11:53:09', '0', '2021-11-28 11:53:09', '春耕计划', '2023-12-15 08:00:00', '13.00', '5', '', '402809817d5c4b34017d5c59d4660002', '402809817d6698ca017d669f5c4c0000', '2021-11-10 08:00:00');
INSERT INTO `scgl_plant_plan` VALUES ('402809817d66abb1017d66cf4b9d0000', '2021-11-28 21:50:01', '0', '2021-11-28 21:50:01', '测试计划2', '2021-11-30 08:00:00', '90.00', '2', '', '402809817d5c4b34017d5c5321730001', '402809817d6693b0017d6695a4780000', '2021-11-11 08:00:00');

-- ----------------------------
-- Table structure for scgl_plant_plane
-- ----------------------------
DROP TABLE IF EXISTS `scgl_plant_plane`;
CREATE TABLE `scgl_plant_plane` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `pick_date` datetime DEFAULT NULL,
  `pre_product` decimal(19,2) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `plant_id` varchar(36) DEFAULT NULL,
  `owner_user_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1355597E3492F59A` (`plant_id`),
  KEY `FK1355597E785DFDAD` (`owner_user_id`),
  CONSTRAINT `FK1355597E3492F59A` FOREIGN KEY (`plant_id`) REFERENCES `zygl_plant_info` (`id`),
  CONSTRAINT `FK1355597E785DFDAD` FOREIGN KEY (`owner_user_id`) REFERENCES `xtgl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of scgl_plant_plane
-- ----------------------------
INSERT INTO `scgl_plant_plane` VALUES ('402809817d6491af017d649d8ed00003', '2021-11-28 11:36:27', '0', '2021-11-28 11:36:27', '春耕计划', null, null, '3', '', '402809817d5c4b34017d5c5321730001', null);

-- ----------------------------
-- Table structure for szda_company_info
-- ----------------------------
DROP TABLE IF EXISTS `szda_company_info`;
CREATE TABLE `szda_company_info` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `contact_phone` varchar(20) DEFAULT NULL,
  `contact_user` varchar(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of szda_company_info
-- ----------------------------
INSERT INTO `szda_company_info` VALUES ('402809817d5a2f38017d5a38f6ba0000', '2021-11-26 13:19:53', '0', '2021-11-26 13:19:56', '天津市和平区', '13454543', 'wx', '梦田科技', '测试', 'www.mengtian-tech.com');
INSERT INTO `szda_company_info` VALUES ('402809817d5a3cea017d5a3de7840000', '2021-11-26 11:15:46', '1', '2021-11-26 13:11:23', '', '', '', 'xx公司', '', 'wwww');
INSERT INTO `szda_company_info` VALUES ('402809817d5a3cea017d5a43eefd0001', '2021-11-26 11:22:21', '1', '2021-11-26 13:11:15', '', '', '', '3333', '', '');

-- ----------------------------
-- Table structure for xtgl_depart
-- ----------------------------
DROP TABLE IF EXISTS `xtgl_depart`;
CREATE TABLE `xtgl_depart` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `parent_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK48DAE912B38A0A63` (`parent_id`),
  CONSTRAINT `FK48DAE912B38A0A63` FOREIGN KEY (`parent_id`) REFERENCES `xtgl_depart` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xtgl_depart
-- ----------------------------
INSERT INTO `xtgl_depart` VALUES ('2c93bcc47d694e2f017d695099a50000', '2021-11-29 09:30:29', '0', '2021-11-29 09:30:29', '研发二部', '', '402809817d6939c7017d693ae0790003');
INSERT INTO `xtgl_depart` VALUES ('402809817d6939c7017d693a292d0000', '2021-11-29 09:05:59', '0', '2021-11-29 09:05:59', '财务部', null, null);
INSERT INTO `xtgl_depart` VALUES ('402809817d6939c7017d693a48e90001', '2021-11-29 09:06:07', '0', '2021-11-29 09:06:07', '人事部', null, null);
INSERT INTO `xtgl_depart` VALUES ('402809817d6939c7017d693a932a0002', '2021-11-29 09:06:26', '0', '2021-11-29 09:06:26', '车间', null, null);
INSERT INTO `xtgl_depart` VALUES ('402809817d6939c7017d693ae0790003', '2021-11-29 09:06:46', '0', '2021-11-29 09:06:46', '研发部', null, null);
INSERT INTO `xtgl_depart` VALUES ('402809817d6939c7017d693b74f40004', '2021-11-29 09:07:24', '0', '2021-11-29 09:07:24', '研发一部', 'test', '402809817d6939c7017d693ae0790003');

-- ----------------------------
-- Table structure for xtgl_role
-- ----------------------------
DROP TABLE IF EXISTS `xtgl_role`;
CREATE TABLE `xtgl_role` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `role_state` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xtgl_role
-- ----------------------------
INSERT INTO `xtgl_role` VALUES ('402809817d8ad235017d8ad2b35f0000', '2021-12-05 21:40:04', '0', '2021-12-05 21:40:04', '管理员', '系统管理员', '1', '1');
INSERT INTO `xtgl_role` VALUES ('402809817d8ad80d017d8ad93a600000', '2021-12-05 21:47:11', '0', '2021-12-05 21:47:11', '人事', '', '2', '1');
INSERT INTO `xtgl_role` VALUES ('402809817d8ad80d017d8ad978f40001', '2021-12-05 21:47:27', '0', '2021-12-05 21:47:27', '财务', '', '3', '1');

-- ----------------------------
-- Table structure for xtgl_user
-- ----------------------------
DROP TABLE IF EXISTS `xtgl_user`;
CREATE TABLE `xtgl_user` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `login_name` varchar(40) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `cellphone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `enter_date` datetime DEFAULT NULL,
  `office_phone` varchar(20) DEFAULT NULL,
  `depart_id` varchar(36) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK987B3C69EF720ED9` (`depart_id`),
  CONSTRAINT `FK987B3C69EF720ED9` FOREIGN KEY (`depart_id`) REFERENCES `xtgl_depart` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xtgl_user
-- ----------------------------
INSERT INTO `xtgl_user` VALUES ('402809817d6693b0017d6695a4780000', '2021-11-28 20:47:02', '0', '2021-11-28 20:47:02', 'admin', '管理员', null, '', null, '', '402809817d6939c7017d693a48e90001', '123456');
INSERT INTO `xtgl_user` VALUES ('402809817d6698ca017d669f5c4c0000', '2021-11-28 20:57:39', '0', '2021-11-28 20:57:39', 'test', '测试1', '13565433245', '34@125.com', '2021-11-27 08:00:00', '056788', '402809817d6939c7017d693a292d0000', '12345');

-- ----------------------------
-- Table structure for zygl_base_info
-- ----------------------------
DROP TABLE IF EXISTS `zygl_base_info`;
CREATE TABLE `zygl_base_info` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `contact_phone` varchar(30) DEFAULT NULL,
  `contact_user` varchar(80) DEFAULT NULL,
  `coordinate_group` longtext,
  `latitude` decimal(20,16) DEFAULT NULL,
  `longitude` decimal(20,16) DEFAULT NULL,
  `mu_number` decimal(19,2) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `view_color` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zygl_base_info
-- ----------------------------
INSERT INTO `zygl_base_info` VALUES ('1', '2021-11-24 09:46:53', '0', '2021-11-24 09:47:37', '13685484574', '路畅', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66967038115439,38.552543797716154],[114.6692996698821,38.55052336380162],[114.6706148925946,38.55119626821458],[114.67107369129664,38.549827519220194],[114.67228821715292,38.55073276999285],[114.67281178422166,38.55052677620954],[114.67397908145398,38.550509610333776],[114.67512062921782,38.55159966014291],[114.67311969175483,38.554167447446446],[114.67453676319472,38.554524263956885],[114.67448578940751,38.55577821923882],[114.67151911514769,38.555808803433365],[114.67098898729408,38.55561510308087],[114.6693785566826,38.554517903387406],[114.66916397996141,38.553548016509396],[114.66967038115439,38.552543797716154]]]}', '38.5528037100229600', '114.6721859360916200', '352.38', '梦田农场', '#914dd5');
INSERT INTO `zygl_base_info` VALUES ('402809817d6ea052017d6ea67ea00000', '2021-11-30 10:22:24', '1', '2021-11-30 16:03:46', '', 'fdsafdsa', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66613165403417,38.554601862848315],[114.66630331541113,38.553056910619446],[114.66818300761973,38.553348734763816],[114.66779676952159,38.55518551162818],[114.66613165403417,38.554601862848315]]]}', '38.5536920575177200', '114.6670500424663600', '45.24', '第二基地', '#127dff');
INSERT INTO `zygl_base_info` VALUES ('402809817d6ea8ad017d6ebbcaf20000', '2021-11-30 10:45:40', '1', '2021-11-30 16:01:47', '1234', 'wxd', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66692129676106,38.552078440967264],[114.66682688247987,38.55058498718422],[114.6684061672788,38.550070003053364],[114.66870657468847,38.5517179523376],[114.66761652520675,38.55245609612753],[114.66692129676106,38.552078440967264]]]}', '38.5513145477088560', '114.6677366876467500', '46.29', '34444', '#127dff');
INSERT INTO `zygl_base_info` VALUES ('402809817d6ea8ad017d6ede5c530001', '2021-11-30 11:23:26', '1', '2021-11-30 15:59:17', '', '', '{\"type\":\"Polygon\",\"coordinates\":[[[114.67555586454567,38.55473919165521],[114.67556444813839,38.55318565632475],[114.67727247897004,38.552576258240116],[114.67716089881309,38.55471344251415],[114.67555586454567,38.55473919165521]]]}', '38.5539238004420900', '114.6761395128344100', '44.06', '3232', '#127dff');
INSERT INTO `zygl_base_info` VALUES ('402809817d6fdbd9017d6ff4e8c50000', '2021-11-30 16:27:41', '0', '2021-11-30 16:27:41', '', '', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66396013735378,38.55478210732686],[114.66583982956239,38.551151468942365],[114.66854349657682,38.55394954958315],[114.66517893319563,38.55527134215295],[114.66404596804226,38.554885104054804],[114.66396013735378,38.55478210732686]]]}', '38.5538293864883200', '114.6660286571425200', '133.55', '4344444', '#127dff');

-- ----------------------------
-- Table structure for zygl_device_info
-- ----------------------------
DROP TABLE IF EXISTS `zygl_device_info`;
CREATE TABLE `zygl_device_info` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `brand_model` varchar(100) DEFAULT NULL,
  `buy_date` datetime DEFAULT NULL,
  `buy_user_name` varchar(100) DEFAULT NULL,
  `coordinate_group` longtext,
  `factory` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `no` varchar(255) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `base_info_id` varchar(36) DEFAULT NULL,
  `device_type_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKF6ACB59CB8F3374B` (`base_info_id`),
  KEY `FKF6ACB59C844AB52B` (`device_type_id`),
  CONSTRAINT `FKF6ACB59C844AB52B` FOREIGN KEY (`device_type_id`) REFERENCES `dict_device_type` (`id`),
  CONSTRAINT `FKF6ACB59CB8F3374B` FOREIGN KEY (`base_info_id`) REFERENCES `zygl_base_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zygl_device_info
-- ----------------------------
INSERT INTO `zygl_device_info` VALUES ('1', '2021-11-26 09:54:12', '0', null, '海康', '2021-11-23 09:53:22', 'xx', '{\"type\":\"Point\",\"coordinates\":[114.67357488956327,38.553431784042374]}', null, '摄像头', '8952-23', null, '1', '1');
INSERT INTO `zygl_device_info` VALUES ('402809817d761e2d017d79290c5a0006', '2021-12-02 11:21:13', '0', '2021-12-02 11:21:13', null, null, null, '{\"type\":\"Point\",\"coordinates\":[114.67260328807627,38.5537778882062]}', null, '摄像头1', null, null, '1', null);
INSERT INTO `zygl_device_info` VALUES ('402809817d761e2d017d792b49810007', '2021-12-02 11:23:39', '0', '2021-12-02 11:23:39', null, null, null, '{\"type\":\"Point\",\"coordinates\":[114.67397657909191,38.552396013925275]}', null, '其他', '003', null, '1', '2');
INSERT INTO `zygl_device_info` VALUES ('402809817d80980f017d80996af20000', '2021-12-03 22:01:17', '0', '2021-12-03 22:01:17', null, null, null, null, null, '摄像头', '123455', null, '1', '2');
INSERT INTO `zygl_device_info` VALUES ('402809817d89ecca017d8a1677290000', '2021-12-05 18:14:27', '0', '2021-12-05 18:14:27', null, null, null, null, null, 'ceewe', '12345454', 'test', '1', '2');
INSERT INTO `zygl_device_info` VALUES ('402809817d89ecca017d8a1a266d0001', '2021-12-05 18:18:29', '0', '2021-12-05 18:18:29', null, null, null, '{\"type\":\"Point\",\"coordinates\":[114.66967646173019,38.55091543463091]}', null, '摄像头2', '002', '大华', '1', '1');

-- ----------------------------
-- Table structure for zygl_facility_info
-- ----------------------------
DROP TABLE IF EXISTS `zygl_facility_info`;
CREATE TABLE `zygl_facility_info` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `coordinate_group` longtext,
  `name` varchar(100) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `base_info_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKF6E6F82FB8F3374B` (`base_info_id`),
  CONSTRAINT `FKF6E6F82FB8F3374B` FOREIGN KEY (`base_info_id`) REFERENCES `zygl_base_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zygl_facility_info
-- ----------------------------
INSERT INTO `zygl_facility_info` VALUES ('1', '2021-11-26 10:22:50', '0', '2021-11-26 10:22:53', '{\"type\":\"Point\",\"coordinates\":[114.671444573127,38.55192394546607]}', '水泵', '测试', '1');

-- ----------------------------
-- Table structure for zygl_field_info
-- ----------------------------
DROP TABLE IF EXISTS `zygl_field_info`;
CREATE TABLE `zygl_field_info` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `coordinate_group` longtext,
  `fzr` varchar(20) DEFAULT NULL,
  `mu_number` float DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `opacity` float DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `view_color` varchar(30) DEFAULT NULL,
  `year_production` float DEFAULT NULL,
  `base_info_id` varchar(36) DEFAULT NULL,
  `type_id` varchar(36) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2D49AF8EB8F3374B` (`base_info_id`),
  KEY `FK2D49AF8E31F043F0` (`type_id`),
  CONSTRAINT `FK2D49AF8E31F043F0` FOREIGN KEY (`type_id`) REFERENCES `dict_field_type` (`id`),
  CONSTRAINT `FK2D49AF8EB8F3374B` FOREIGN KEY (`base_info_id`) REFERENCES `zygl_base_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zygl_field_info
-- ----------------------------
INSERT INTO `zygl_field_info` VALUES ('1', null, '0', null, '{\"type\":\"Polygon\",\"coordinates\":[[[114.67027727654953,38.554404452166594],[114.67003695035986,38.55341739924911],[114.67133299362489,38.55282516736766],[114.67129007828065,38.55419845838328],[114.67035452364529,38.5547477749205],[114.67027727654953,38.554404452166594]]]}', 'Jon', '12', '大白菜', '0.1', '136854545', '#e36b77', '12', '1', '1', null);
INSERT INTO `zygl_field_info` VALUES ('402809817d6fdbd9017d75eafc43000d', '2021-12-01 20:14:34', '1', '2021-12-01 20:30:20', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66540209318212,38.55299682907203],[114.66599432473616,38.55178661649548],[114.66619173558159,38.5527307537413],[114.66553083921484,38.55291958132143],[114.66540209318212,38.55299682907203]]]}', '', '5.59', '434343', '0.1', '', '#e36b77', null, '402809817d6fdbd9017d6ff4e8c50000', '2', null);
INSERT INTO `zygl_field_info` VALUES ('402809817d6fdbd9017d75ebf91b000e', '2021-12-01 20:15:38', '1', '2021-12-01 20:31:53', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66430293938477,38.55431725999287],[114.66453897377808,38.5538795234489],[114.66497241885311,38.55392243879314],[114.66495096118099,38.554201388530686],[114.66430293938477,38.55431725999287]]]}', '', '2.74', '4455', '0.1', '', '#e36b77', null, '402809817d6fdbd9017d6ff4e8c50000', null, null);
INSERT INTO `zygl_field_info` VALUES ('402809817d6fdbd9017d75ec33c4000f', '2021-12-01 20:15:53', '1', '2021-12-05 18:27:14', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66478788306934,38.55503394627439],[114.66477071686616,38.55466058274678],[114.66494666971205,38.55463483360572],[114.66512691402689,38.554939532484326],[114.6649123373057,38.55508973618916],[114.66478788306934,38.55503394627439]]]}', '', '1.57', '444444444444', '0.1', '', '#e36b77', null, '402809817d6fdbd9017d6ff4e8c50000', null, null);
INSERT INTO `zygl_field_info` VALUES ('402809817d6fdbd9017d75edd5ab0010', '2021-12-01 20:17:40', '1', '2021-12-05 18:27:08', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66545788260579,38.55478210699943],[114.66547075734003,38.55434007898652],[114.66563383545169,38.5542799974391],[114.66561666957593,38.55478639846837],[114.66545788260579,38.55478210699943]]]}', 'wxd', '1.1', '辣椒', '0.1', '45454', '#e36b77', null, '402809817d6fdbd9017d6ff4e8c50000', '1', '日日日');
INSERT INTO `zygl_field_info` VALUES ('402809817d6fdbd9017d75ee06800011', '2021-12-01 20:17:53', '1', '2021-12-05 18:27:00', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66570679173334,38.55477781553049],[114.66571108320228,38.55433149588493],[114.66586128690712,38.55430145511122],[114.6659084937203,38.55479069010102],[114.66570679173334,38.55477781553049]]]}', '', '1.2', '455555555555', '0.1', '', '#e36b77', null, '402809817d6fdbd9017d6ff4e8c50000', null, null);

-- ----------------------------
-- Table structure for zygl_partition_info
-- ----------------------------
DROP TABLE IF EXISTS `zygl_partition_info`;
CREATE TABLE `zygl_partition_info` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `coordinate_group` longtext,
  `mu_number` decimal(19,2) DEFAULT NULL,
  `name` varchar(120) DEFAULT NULL,
  `opacity` float DEFAULT NULL,
  `view_color` varchar(50) DEFAULT NULL,
  `base_id` varchar(36) DEFAULT NULL,
  `sub_type_id` varchar(36) DEFAULT NULL,
  `type_id` varchar(36) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKA5511E1EBF4F1B6` (`base_id`),
  KEY `FKA5511E1EF9BAAFE0` (`type_id`),
  KEY `FKA5511E1EEC9FC4B5` (`sub_type_id`),
  CONSTRAINT `FKA5511E1EBF4F1B6` FOREIGN KEY (`base_id`) REFERENCES `zygl_base_info` (`id`),
  CONSTRAINT `FKA5511E1EEC9FC4B5` FOREIGN KEY (`sub_type_id`) REFERENCES `dict_partition_sub_type` (`id`),
  CONSTRAINT `FKA5511E1EF9BAAFE0` FOREIGN KEY (`type_id`) REFERENCES `dict_partition_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zygl_partition_info
-- ----------------------------
INSERT INTO `zygl_partition_info` VALUES ('1', '2021-11-25 08:40:50', '0', '2021-11-25 08:40:54', '{\"type\":\"Polygon\",\"coordinates\":[[[114.67155615328396,38.55564041437533],[114.67198530672634,38.554850771975865],[114.67262045395204,38.55449886612037],[114.67293802723746,38.554344370946595],[114.6739422460307,38.554601863012024],[114.67395082962342,38.55538292247361],[114.6735388420568,38.55565758025109],[114.6731783527723,38.55571766196222],[114.67227713054329,38.55570907886063],[114.67155615328396,38.55564041437533]]]}', '34.89', '生活区', '0.1', '#FFFFFF', '1', '1', '1', 'test');
INSERT INTO `zygl_partition_info` VALUES ('402809817d6fdbd9017d73e378e60002', '2021-12-01 10:47:07', '1', '2021-12-01 19:41:21', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66541067612,38.55344314871759],[114.66588274490663,38.55250759342739],[114.66626040006689,38.55303974441627],[114.66541067612,38.55344314871759]]]}', '4.38', 'ddd', '0.1', '#FFFFFF', '402809817d6fdbd9017d6ff4e8c50000', '4', '2', null);
INSERT INTO `zygl_partition_info` VALUES ('402809817d6fdbd9017d73f4721a0003', '2021-12-01 11:05:39', '1', '2021-12-01 11:24:18', '{\"type\":\"Polygon\",\"coordinates\":[[[114.6665264750702,38.55352897907865],[114.66728178506331,38.55363197564289],[114.66685263162093,38.5527994182266],[114.66650930886702,38.55281658410236],[114.6665264750702,38.55352897907865]]]}', '6.02', '4444', '0.1', '#FFFFFF', '402809817d6fdbd9017d6ff4e8c50000', '4', '2', null);
INSERT INTO `zygl_partition_info` VALUES ('402809817d6fdbd9017d73f8f6eb0004', '2021-12-01 11:10:35', '1', '2021-12-01 11:12:56', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66485277697234,38.55449028301878],[114.6649471905987,38.5539924650911],[114.66544500852638,38.55449028301878],[114.66485277697234,38.55449028301878]]]}', '2.14', '3232', '0.1', '#FFFFFF', '402809817d6fdbd9017d6ff4e8c50000', '4', '2', null);
INSERT INTO `zygl_partition_info` VALUES ('402809817d6fdbd9017d740607eb0005', '2021-12-01 11:24:52', '1', '2021-12-05 18:26:45', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66499010594292,38.55442161836977],[114.6650759366314,38.55366630837666],[114.66547075766742,38.554146960101164],[114.66528193008729,38.55449886612037],[114.66499010594292,38.55442161836977]]]}', '3.31', '加工区', '0.1', '#ddd0d0', '402809817d6fdbd9017d6ff4e8c50000', '4', '2', 'rrrr');
INSERT INTO `zygl_partition_info` VALUES ('402809817d6fdbd9017d744853bd0006', '2021-12-01 12:37:16', '1', '2021-12-01 20:46:57', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66688696402728,38.553649142173484],[114.6669470455747,38.5532886532164],[114.66745344676768,38.55348606406183],[114.66717878817165,38.55374355612726],[114.66688696402728,38.553649142173484]]]}', '2.14', '3232', '0.1', '#FFFFFF', '402809817d6fdbd9017d6ff4e8c50000', null, null, null);
INSERT INTO `zygl_partition_info` VALUES ('402809817d6fdbd9017d7448c7290007', '2021-12-01 12:37:46', '1', '2021-12-01 13:09:44', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66669813611975,38.55431003821282],[114.66669813611975,38.55390663423891],[114.66704145920107,38.553923800114674],[114.66705004213895,38.55426712286858],[114.66669813611975,38.55431003821282]]]}', '1.88', 'test', '0.1', '#FFFFFF', '402809817d6fdbd9017d6ff4e8c50000', null, null, null);
INSERT INTO `zygl_partition_info` VALUES ('402809817d6fdbd9017d744ebd6e0008', '2021-12-01 12:44:17', '1', '2021-12-01 19:41:30', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66642347850596,38.552842333898255],[114.66649214331868,38.55255050942647],[114.66684404901046,38.552825167695076],[114.66642347850596,38.552842333898255]]]}', '0.88', '3232', '0.1', '#FFFFFF', '402809817d6fdbd9017d6ff4e8c50000', null, '2', null);
INSERT INTO `zygl_partition_info` VALUES ('402809817d6fdbd9017d74514c5a0009', '2021-12-01 12:47:04', '1', '2021-12-01 20:46:53', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66420046288862,38.55478210732686],[114.66434637512451,38.55445595077613],[114.6645609518457,38.554619029051494],[114.66420046288862,38.55478210732686]]]}', '0.68', '3243', '0.1', '#FFFFFF', '402809817d6fdbd9017d6ff4e8c50000', null, null, null);
INSERT INTO `zygl_partition_info` VALUES ('402809817d6fdbd9017d745e06ba000a', '2021-12-01 13:00:58', '1', '2021-12-01 19:40:58', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66576687344447,38.55449028301879],[114.66586128723453,38.55415983483541],[114.66608015558838,38.55421991638283],[114.66604582318202,38.554516032159846],[114.66576687344447,38.55449028301879]]]}', '1.17', '3232', '0.1', '#FFFFFF', '402809817d6fdbd9017d6ff4e8c50000', null, null, null);
INSERT INTO `zygl_partition_info` VALUES ('402809817d6fdbd9017d7460c0e6000b', '2021-12-01 13:03:57', '1', '2021-12-01 19:41:26', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66608552000638,38.553592279473335],[114.66605547923267,38.553356245080025],[114.66620139130485,38.55322320741466],[114.66623143207856,38.55346782490956],[114.66608552000638,38.553592279473335]]]}', '0.56', '3243', '0.1', '#FFFFFF', '402809817d6fdbd9017d6ff4e8c50000', '2', '1', null);
INSERT INTO `zygl_partition_info` VALUES ('402809817d6fdbd9017d746105d8000c', '2021-12-01 13:04:15', '1', '2021-12-05 18:26:51', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66634301207179,38.55366094395861],[114.66638592757974,38.553343370509474],[114.66653183981563,38.55344207576848],[114.66652754801927,38.55368240163073],[114.66634301207179,38.55366094395861]]]}', '0.69', '32443', '0.1', '#FFFFFF', '402809817d6fdbd9017d6ff4e8c50000', '2', '1', '4444');
INSERT INTO `zygl_partition_info` VALUES ('402809817d8ad80d017d8aec82c40002', '2021-12-05 22:08:15', '0', '2021-12-05 22:08:15', '{\"type\":\"Polygon\",\"coordinates\":[[[114.66553083921485,38.55257625840382],[114.66624323386372,38.55255909252806],[114.66642347817856,38.55318565648845],[114.66547075766744,38.55322857183269],[114.66553083921485,38.55257625840382]]]}', '7.75', 'iii', null, null, '402809817d6fdbd9017d6ff4e8c50000', null, null, null);

-- ----------------------------
-- Table structure for zygl_plant_info
-- ----------------------------
DROP TABLE IF EXISTS `zygl_plant_info`;
CREATE TABLE `zygl_plant_info` (
  `id` varchar(36) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `parent_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4F9E0E02408AAAFB` (`parent_id`),
  CONSTRAINT `FK4F9E0E02408AAAFB` FOREIGN KEY (`parent_id`) REFERENCES `zygl_plant_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zygl_plant_info
-- ----------------------------
INSERT INTO `zygl_plant_info` VALUES ('402809817d5c4b34017d5c4f49a10000', '2021-11-26 20:53:59', '0', '2021-11-26 20:53:59', '玉米', null);
INSERT INTO `zygl_plant_info` VALUES ('402809817d5c4b34017d5c5321730001', '2021-11-26 20:58:11', '0', '2021-11-26 20:58:11', '登海618', '402809817d5c4b34017d5c4f49a10000');
INSERT INTO `zygl_plant_info` VALUES ('402809817d5c4b34017d5c59d4660002', '2021-11-26 21:05:30', '0', '2021-11-26 21:05:30', '京科968', null);
INSERT INTO `zygl_plant_info` VALUES ('402809817d5c4b34017d5c5bf3b00003', '2021-11-26 21:07:49', '0', '2021-11-26 21:07:49', '中单909', null);
INSERT INTO `zygl_plant_info` VALUES ('402809817d5c4b34017d5c642a3f0004', '2021-11-26 21:16:48', '1', '2021-11-27 20:48:41', 'eee', '402809817d5c4b34017d5c4f49a10000');
